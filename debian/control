Source: trinityrnaseq
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Michael R. Crusoe <crusoe@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libjung-free-java (>= 2.1.1),
               javahelper,
               libgetopt-java,
               default-jdk,
               libjs-jquery,
               jaligner,
               libhts-dev,
               zlib1g-dev,
               cmake
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/trinityrnaseq
Vcs-Git: https://salsa.debian.org/med-team/trinityrnaseq.git
Homepage: https://github.com/trinityrnaseq/trinityrnaseq
Rules-Requires-Root: no

Package: trinityrnaseq
Architecture: amd64 arm64 ppc64el riscv64 loong64
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${perl:Depends},
         ${java:Depends},
         bowtie,
         bowtie2,
         libwww-perl,
         default-jre-headless,
         samtools,
         jellyfish,
         r-base-core,
         rsem,
         berkeley-express,
         trimmomatic,
         parafly,
         ncbi-blast+,
         python3,
         liburi-perl,
         python3-htseq,
         subread,
         kallisto
Recommends: ${java:Recommends},
            curl,
            trinityrnaseq-examples,
            picard-tools,
            tabix,
            gmap,
            salmon,
            rna-star,
            hisat2,
            r-cran-tidyverse,
            r-cran-readr,
            r-bioc-edger,
            r-bioc-deseq2,
            r-bioc-rots,
            r-cran-cluster,
            r-cran-fastcluster,
            r-bioc-ctc,
            r-bioc-goseq,
            r-cran-goplot,
            r-cran-gplots,
            r-bioc-dexseq,
            r-cran-ape,
            r-bioc-biobase,
            r-bioc-qvalue,
            r-cran-argparse,
            r-cran-kernsmooth,
            python3-numpy,
            python3-hisat2
Suggests: collectl,
          transdecoder,
          r-bioc-tximport,
          r-bioc-tximportdata
Description: RNA-Seq De novo Assembly
 Trinity represents a novel method for the efficient and robust de novo
 reconstruction of transcriptomes from RNA-seq data. Trinity combines three
 independent software modules: Inchworm, Chrysalis, and Butterfly, applied
 sequentially to process large volumes of RNA-seq reads. Trinity partitions
 the sequence data into many individual de Bruijn graphs, each representing the
 transcriptional complexity at a given gene or locus, and then processes
 each graph independently to extract full-length splicing isoforms and to tease
 apart transcripts derived from paralogous genes.

Package: trinityrnaseq-examples
Architecture: amd64 arm64 ppc64el riscv64 loong64
Depends: ${perl:Depends},
         ${java:Depends},
         ${misc:Depends},
         r-base-core
Recommends: trinityrnaseq
Description: RNA-Seq De novo Assembly common example and testing files
 Trinity represents a novel method for the efficient and robust de novo
 reconstruction of transcriptomes from RNA-seq data. Trinity combines three
 independent software modules: Inchworm, Chrysalis, and Butterfly, applied
 sequentially to process large volumes of RNA-seq reads. Trinity partitions
 the sequence data into many individual de Bruijn graphs, each representing the
 transcriptional complexity at a given gene or locus, and then processes
 each graph independently to extract full-length splicing isoforms and to tease
 apart transcripts derived from paralogous genes.
 .
 This package contains testing & example files.
